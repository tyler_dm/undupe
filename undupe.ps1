﻿

param
(
    [string]$target
)

$working_dir = Get-Location
$target_dir = "."

$file_hashes = New-Object System.Collections.ArrayList
$duplicates = New-Object System.Collections.ArrayList

function GetFileHash
{
    param ([string] $path)

    return Get-FileHash -Path $path -Algorithm MD5
}

function CompareName
{
    param
    (
        [string] $name1,
        [string] $name2
    )

    return $name1.ToUpper() -eq $name2.ToUpper()
}

function FindDupes
{
    param ([string] $path)

    $items = Get-ChildItem -Path $path | Sort-Object
    $num_items = ($items | measure).Count
    $count = 0

    #write-output "Path: $path"

    $perc = 0

    foreach ($item in $items)
    {
        try
        {
            #write-output "Item: $item"
        
            #$full_path = (Join-Path -Path "$path" -ChildPath "$item" -Resolve)
            $full_path = Join-Path -Path $path -ChildPath $item

            if ((Get-Item -LiteralPath $full_path) -is [System.IO.DirectoryInfo])
            {
                # item is a directory
                Write-Host "`r $full_path\"
            
                # recurse through directories
                FindDupes $full_path

                #Write-Output ""
            }
            else
            {
                # $item is a file
               
                $hash = GetFileHash $full_path

                if ($file_hashes.IndexOf($hash.Hash) -lt 0)
                {
                    #Write-host "Adding $hash.Hash to file_hashes"
                    [void]$file_hashes.Add($hash.Hash)
                }
                else
                {
                    #write "adding to duplicates"
                    # File is already present, add $file to $duplicates

                    [void]$duplicates.Add($hash)

                    # Write-output $duplicates
                }
                $count++
            }

            if ($num_items -eq 0)
            {
                $perc = 100
            }
            else
            {
                $perc = [math]::Round($count/$num_items * 100, 2)
            }

            Write-Progress -Activity Searching -Status "$path" -PercentComplete $perc -CurrentOperation $item
        }
        catch [System.Management.Automation.WildcardPatternException]
        {
            "`n  A file could not be processed due to a its name containing one or more invalid characters.`n"
        }
    }
}

if ($target)
{
    if (-NOT (Test-Path $target))
    {
        Write-Output "Could not locate $target"
    }
    else
    {
        #$target_dir = Join-Path -Path (Resolve-Path $working_dir -Relative) -ChildPath $target -Resolve
        $target_dir = Resolve-Path $target
    }
}
else
{
    $target_dir = $working_dir
}

Write-Output "`nSearching for duplicate files in $target_dir"

FindDupes $target_dir

$count = 0
$size = 0

foreach ($dupe in $duplicates)
{
    if (-NOT($dupe -eq $null))
    {
        if (Test-Path $dupe.Path)
        {
            $count++
            $size += (Get-Item $dupe.Path).Length
            write-host $dupe.Hash,$dupe.Path
            }
    }
}

if ($count -gt 0)
{
    if ($size -ge 1TB)
    {
        $size = $size / 1TB

        $unit = "TB"
    }
    ElseIf ($size -ge 1GB)
    {
        $size = $size / 1GB

        $unit = "GB"
        
    }
    ElseIf ($size -ge 1MB)
    {
        $size = $size / 1MB

        $unit = "MB"
    }
    ElseIf ($size -ge 1KB)
    {
        $size = $size / 1KB

        $unit = "KB"
    }
    ElseIf ($size -ge 0)
    {
        $unit = "B"
    }

    Write-Host "`nFound ($count) duplicate files."
    $size_rounded = [math]::Round($size,2)
    $delete = Read-Host -Prompt "Do you want to delete ($count) files? This will free approximately $size_rounded $unit [Y\N]"

    if ($delete.ToUpper() -eq "Y")
    {
        $confirm = Read-Host -Prompt "($count) files will be moved to the Recycle Bin. Are your sure? [Y/N]"

        if ($confirm.ToUpper() -eq "Y")
        {
            Write-Host "`nMoving files to Recycle Bin..."

            foreach ($dupe in $duplicates)
            {

                if(-NOT($dupe -eq $null))
                {
                    if (Test-Path $dupe.Path)
                    {
                        Write-host $dupe.Path
                
                        Add-Type -AssemblyName Microsoft.VisualBasic
                        [Microsoft.VisualBasic.FileIO.FileSystem]::DeleteFile($dupe.Path,'OnlyErrorDialogs','SendToRecycleBin')
                    }
               }
            }
        }
    }
}
else
{
    Write-Host "`nNo duplicates found."
}
# SIG # Begin signature block
# MIIMoAYJKoZIhvcNAQcCoIIMkTCCDI0CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUDV+rzHjtPciZ9Wp/9OMIhS01
# SwygggfiMIIDQTCCAi2gAwIBAgIQlBFdR1PrQqFJaQnrmR+XCTAJBgUrDgMCHQUA
# MCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdDAe
# Fw0xODA3MzAyMDQ1NDVaFw0zOTEyMzEyMzU5NTlaMB0xGzAZBgNVBAMTElRETSBQ
# b3dlclNoZWxsIENTQzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK3n
# xG7utmFQtox6jRdzBec5qeuqXPXYR+hhR1pcE+FffcNxu4Ls/azdPET6aiy0xo0U
# Z6UWVHYvguuWaQmSZuc8cCM6nkCUXZzHDY8qjDFp93nK7FaGQ8voJoAEJTBYoG5A
# TNQuUR92n8ubgVaP9BtapoLKQZt0Nm9rd3Dnr0BY6kbLJj4dOqkLKrYvbpwjAcdX
# vHIoAFB6MLNbPBzG6dZ4ePV7Ry2KJk/GqDflX0iNMe4rd8kmZ0M39qqx9dLdRmiH
# /j6APQLaPbd4F29/lt27jDFpgfVdu8eSgte0+FfrRTy0wKuVjILs78Qhlsihhow/
# HJSYMZ/AjghEtJ+4i6UCAwEAAaN2MHQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwXQYD
# VR0BBFYwVIAQkGlM+gwi5F9GHTyuzfLS66EuMCwxKjAoBgNVBAMTIVBvd2VyU2hl
# bGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdIIQ0SBOnyyd+aVPl5Vw0ef6CDAJBgUr
# DgMCHQUAA4IBAQCZv4ca/96LLpNnz48gvQHtoKtELUFgQnrXtX1f7Ap2MWifjXoK
# lTYjq0Ff5O6teQPmjgNMix7QzKJ+GMA5x0LUKZRd1l/kTkc2QJg/sE2sp3a7XPsq
# mIvJF22/8UwUFoiJ9JosLQHt9Qt3JVBPiKcKpjpfFUk3hXvTrGMpGyw+p7R6lo6i
# tfCAQQwFDv2eLudKbZrJoTzPDd6zM9sjsVYe4RbOZ8psmC6npG6EEximWKKYHYh3
# yJ0r3WIGBbKTKAUQ0NdZRxwH/hdBZbOuqRWAG/HPf6HpPeyc9hFqufwKnWEjkzz7
# pCXbg0ZlUx14nRLHixWqmc807v3ibV4kbD+jMIIEmTCCA4GgAwIBAgIPFojwOSVe
# Y45pFDkH5jMLMA0GCSqGSIb3DQEBBQUAMIGVMQswCQYDVQQGEwJVUzELMAkGA1UE
# CBMCVVQxFzAVBgNVBAcTDlNhbHQgTGFrZSBDaXR5MR4wHAYDVQQKExVUaGUgVVNF
# UlRSVVNUIE5ldHdvcmsxITAfBgNVBAsTGGh0dHA6Ly93d3cudXNlcnRydXN0LmNv
# bTEdMBsGA1UEAxMUVVROLVVTRVJGaXJzdC1PYmplY3QwHhcNMTUxMjMxMDAwMDAw
# WhcNMTkwNzA5MTg0MDM2WjCBhDELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0
# ZXIgTWFuY2hlc3RlcjEQMA4GA1UEBxMHU2FsZm9yZDEaMBgGA1UEChMRQ09NT0RP
# IENBIExpbWl0ZWQxKjAoBgNVBAMTIUNPTU9ETyBTSEEtMSBUaW1lIFN0YW1waW5n
# IFNpZ25lcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOnpPd/XNwjJ
# HjiyUlNCbSLxscQGBGue/YJ0UEN9xqC7H075AnEmse9D2IOMSPznD5d6muuc3qaj
# DjscRBh1jnilF2n+SRik4rtcTv6OKlR6UPDV9syR55l51955lNeWM/4Og74iv2MW
# LKPdKBuvPavql9LxvwQQ5z1IRf0faGXBf1mZacAiMQxibqdcZQEhsGPEIhgn7ub8
# 0gA9Ry6ouIZWXQTcExclbhzfRA8VzbfbpVd2Qm8AaIKZ0uPB3vCLlFdM7AiQIiHO
# IiuYDELmQpOUmJPv/QbZP7xbm1Q8ILHuatZHesWrgOkwmt7xpD9VTQoJNIp1KdJp
# rZcPUL/4ygkCAwEAAaOB9DCB8TAfBgNVHSMEGDAWgBTa7WR0FJwUPKvdmam9WyhN
# izzJ2DAdBgNVHQ4EFgQUjmstM2v0M6eTsxOapeAK9xI1aogwDgYDVR0PAQH/BAQD
# AgbAMAwGA1UdEwEB/wQCMAAwFgYDVR0lAQH/BAwwCgYIKwYBBQUHAwgwQgYDVR0f
# BDswOTA3oDWgM4YxaHR0cDovL2NybC51c2VydHJ1c3QuY29tL1VUTi1VU0VSRmly
# c3QtT2JqZWN0LmNybDA1BggrBgEFBQcBAQQpMCcwJQYIKwYBBQUHMAGGGWh0dHA6
# Ly9vY3NwLnVzZXJ0cnVzdC5jb20wDQYJKoZIhvcNAQEFBQADggEBALozJEBAjHzb
# WJ+zYJiy9cAx/usfblD2CuDk5oGtJoei3/2z2vRz8wD7KRuJGxU+22tSkyvErDmB
# 1zxnV5o5NuAoCJrjOU+biQl/e8Vhf1mJMiUKaq4aPvCiJ6i2w7iH9xYESEE9XNjs
# n00gMQTZZaHtzWkHUxY93TYCCojrQOUGMAu4Fkvc77xVCf/GPhIudrPczkLv+XZX
# 4bcKBUCYWJpdcRaTcYxlgepv84n3+3OttOe/2Y5vqgtPJfO44dXddZhogfiqwNGA
# wsTEOYnB9smebNd0+dmX+E/CmgrNXo/4GengpZ/E8JIh5i15Jcki+cPwOoRXrToW
# 9GOUEB1d0MYxggQoMIIEJAIBATBAMCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9j
# YWwgQ2VydGlmaWNhdGUgUm9vdAIQlBFdR1PrQqFJaQnrmR+XCTAJBgUrDgMCGgUA
# oHgwGAYKKwYBBAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYB
# BAGCNwIBBDAcBgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0B
# CQQxFgQUfTkHp9EJVw/OuBCzzhrGiGM/4UwwDQYJKoZIhvcNAQEBBQAEggEAqE/Z
# lt+bKcKDdBsCCkZlI8N/3NdiUwLw3tytXgerdZG/GxBnD/G9vI8J9J/OHT3lySOY
# PI0r1KrQJ1BRJMLD/PI18Ay8ClChoGYw166RHpFeU+d7rxJCed7+PNlSMCMNWPXB
# lvDbexq3BcWBeXiDoPEi8YJyVnWQYP7AvrZ+KO9YJy8NDyUZM6ZMdWAMDFDT7FLe
# nSsAoe9vnjK9LeftuGoOXM7DB5+h7Lcc7kUlK3GLMuS72SJPGpGgw89z+NYZ6GZ+
# WOkQ6y51sfbtM0mMeEyv5n6GmPsvHEeO+RM6vcbnV/tceJoh0GsNTyHu2i1lATZa
# g7KphnRDKIjStVnTF6GCAkMwggI/BgkqhkiG9w0BCQYxggIwMIICLAIBATCBqTCB
# lTELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAlVUMRcwFQYDVQQHEw5TYWx0IExha2Ug
# Q2l0eTEeMBwGA1UEChMVVGhlIFVTRVJUUlVTVCBOZXR3b3JrMSEwHwYDVQQLExho
# dHRwOi8vd3d3LnVzZXJ0cnVzdC5jb20xHTAbBgNVBAMTFFVUTi1VU0VSRmlyc3Qt
# T2JqZWN0Ag8WiPA5JV5jjmkUOQfmMwswCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJ
# AzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTE4MDczMDIwNDk1OFowIwYJ
# KoZIhvcNAQkEMRYEFIAIiVxw4sRTMmf8VlHNk5qgIUKSMA0GCSqGSIb3DQEBAQUA
# BIIBAIjU5PQWiUljWPU32/BX558FN1YP2HIUhVgyi+kEIb4iM6mvuI5v0w3Mj0Dj
# rwCvueBl1BChzRyS401Ix1m5jYWx1l5Ln01/na0u1tI3MQc7Udpd+xFixdFkKkdd
# yhjNaWTQLhx1SR3tPifAOt1BaFzrlMQdCZhxNvD5GEWcdLfR7p97rcBbqrJWvXOc
# 2zpiv+8J/cLgucUqkcHDfBz2gxCpO8EfVy219E9CdGqpS9HrZZ9NAN1+pZqaLAQp
# kgDqebQhwHBS/dyFopoF870Y5NJvap/6jU6EVayGUcvQ57FVTclRl61edaW0DPoe
# utZG62BT381RkXbAc82pmY7ppR4=
# SIG # End signature block
