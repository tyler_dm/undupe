# undupe

### WARNING: THIS SCRIPT DELETES FILES FROM YOUR SYSTEM. IF YOU ARE NOT COMFORTABLE WITH THAT, DO NOT USE THIS SCRIPT. I AM NOT RESPONSIBLE FOR ANY LOSS OF YOUR DATA. BY USING THIS PROGRAM YOU ACKNOWLEDGE THAT YOUR DATA IS YOUR RESONSIBILTY, AND YOURS ALONE.

A duplicate file finder written in PowerShell. Undupe recursively deletes duplicate files found in the specified directory.



~~~
Usage:

.\undupe -target X:\Path\To\Target\Directory
~~~

If no target is specified, `undupe` will run on the current working directory.

